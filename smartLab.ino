#include <dht.h>
#include <stdbool.h>

#include <Wire.h> // Library for I2C communication
#include <LiquidCrystal_I2C.h> // Library for LCD
#include <ArduinoJson.h>

#define DHT_SENSOR_PIN A0
#define MQ_SENSOR_PIN A1
#define SDA_PIN A4
#define SCL_PIN A5

int OPTIMAL_STATE_PIN = 8;
int ALARM_PIN = 9;
int FLAME_PIN = 4;
dht DHT;
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);
char alarmNotifications[4][16]; 

String message = "";
bool messageReady = false;
 
void setup(){
 
  Serial.begin(9600);
  delay(500);//Delay to let system boot
  
  pinMode(ALARM_PIN, OUTPUT);
  pinMode(OPTIMAL_STATE_PIN, OUTPUT);
  pinMode(FLAME_PIN, INPUT);

  lcd.clear();
  lcd.init();
  lcd.backlight();
  
  digitalWrite(ALARM_PIN, LOW);
  digitalWrite(OPTIMAL_STATE_PIN, LOW);

  strcpy(alarmNotifications[0], "High Temperature");
  strcpy(alarmNotifications[1], "High Humidity");
  strcpy(alarmNotifications[2], "High CO2 gas");
  strcpy(alarmNotifications[3], "Flame alarm");
  
  delay(1000);
}
 
void loop(){

  lcd.clear();

  float tempIntervalStart = 15.00;
  float tempIntervalEnd = 30.00; // Should be 25.00

  DHT.read11(DHT_SENSOR_PIN);
  int temperature = DHT.temperature;
  int humidity = DHT.humidity;

  int flameValue = digitalRead(FLAME_PIN);

  int gas = analogRead(MQ_SENSOR_PIN);


  bool shouldActivateAlarm = true;
  char alarmMessage[16] = ""; 
  int alarmValue = 0;
  if((temperature < tempIntervalStart) || (temperature > tempIntervalEnd))  {
    strcpy(alarmMessage, alarmNotifications[0]); 
    alarmValue = 1;
  } else if (humidity > 80) {
    strcpy(alarmMessage, alarmNotifications[1]);
    alarmValue = 2;
  } else if (gas > 200) {
    strcpy(alarmMessage, alarmNotifications[2]);
    alarmValue = 3;
  } else if (flameValue == HIGH) {
    strcpy(alarmMessage, alarmNotifications[3]);
    alarmValue = 4;
  } else {
     shouldActivateAlarm = false;
  }
  
  if (shouldActivateAlarm) {
    digitalWrite(ALARM_PIN, HIGH);
    digitalWrite(OPTIMAL_STATE_PIN, LOW);
    // Serial.println("Alarm ON");
  
    lcd.setCursor(0,0);
    lcd.print(alarmMessage);
  } else {
    digitalWrite(ALARM_PIN, LOW);
    digitalWrite(OPTIMAL_STATE_PIN, HIGH);
    // Serial.println("Alarm OFF");

    lcd.setCursor(0,0);
    lcd.print("Te: ");
    lcd.print(temperature);
    lcd.print("C");
    lcd.print(" Hu: ");
    lcd.print(humidity);
    lcd.print("%");
    lcd.setCursor(0,1);
    lcd.print("CO2: ");
    lcd.print(gas);
    lcd.print("ppm");
  }
  
//  Serial.print("Current humidity = ");
//  Serial.print(humidity);
//  Serial.print("%  ");
//  Serial.print("temperature = ");
//  Serial.print(temperature); 
//  Serial.println("C  ");
//  Serial.print("CO2 value = ");
//  Serial.print(gas);
//  Serial.println(" PPM");
  
  DynamicJsonDocument doc(1024);
  doc["temperature"] = temperature;
  doc["humidity"] = humidity;
  doc["gas"] = gas;
  doc["alarm"] = alarmValue;
  serializeJson(doc,Serial);
      
  delay(4000);
}
